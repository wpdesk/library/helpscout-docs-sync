<?php

namespace WPDesk\HsSync;

use WPDesk\HsSync\Shortcodes\Box;
use WPDesk\HsSync\Shortcodes\Button;
use WPDesk\HsSync\Shortcodes\Div;
use WPDesk\HsSync\Shortcodes\Lightbox;

/**
 * WordPress Shortcodes. Use to parse WordPress style shortcodes in text content.
 *
 * To add more shortcodes put more callbacks into $this->shortcodes in __construct
 *
 * @package WPDesk\HsSync
 */
class ShortCodeParser
{
    private array $shortcodes;

    public function __construct()
    {
    	$box = new Box();
    	$div = new Div();
	    $button = new Button();
	    $lightbox = new Lightbox();
        $this->shortcodes = [
            'lightbox' => [ $lightbox, 'do_shortcode' ],
            'button'   => [ $button, 'do_shortcode' ],
            'box'      => [ $box, 'do_shortcode' ],
            'div'      => [ $div, 'do_shortcode' ],
            'youtube'  => static function ($atts, $content = null) {

            }
        ];
    }

    public function doShortcodes(string $content): string
    {
        if (false === strpos($content, '[')) {
            return $content;
        }

        if (empty($this->shortcodes) || !is_array($this->shortcodes)) {
            return $content;
        }

        // Find all registered tag names in $content.
        preg_match_all('@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches);
        $tagnames = array_intersect(array_keys($this->shortcodes), $matches[1]);

        if (empty($tagnames)) {
            return $content;
        }

        $shortcode_parse_atts = function($text) {
	        $atts = array();
	        $pattern = '/([\w-]+)\s*=\s*"([^"]*)"(?:\s|$)|([\w-]+)\s*=\s*\'([^\']*)\'(?:\s|$)|([\w-]+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|\'([^\']*)\'(?:\s|$)|(\S+)(?:\s|$)/';
	        $text = preg_replace("/[\x{00a0}\x{200b}]+/u", ' ', $text);
	        if (preg_match_all($pattern, $text, $match, PREG_SET_ORDER)) {
		        foreach ($match as $m) {
			        if (!empty($m[1])) {
				        $atts[strtolower($m[1])] = stripcslashes($m[2]);
			        } elseif (!empty($m[3])) {
				        $atts[strtolower($m[3])] = stripcslashes($m[4]);
			        } elseif (!empty($m[5])) {
				        $atts[strtolower($m[5])] = stripcslashes($m[6]);
			        } elseif (isset($m[7]) && strlen($m[7])) {
				        $atts[] = stripcslashes($m[7]);
			        } elseif (isset($m[8]) && strlen($m[8])) {
				        $atts[] = stripcslashes($m[8]);
			        } elseif (isset($m[9])) {
				        $atts[] = stripcslashes($m[9]);
			        }
		        }

		        // Reject any unclosed HTML elements.
		        foreach ($atts as &$value) {
			        if (false !== strpos($value, '<')) {
				        if (1 !== preg_match('/^[^<]*+(?:<[^>]*+>[^<]*+)*+$/', $value)) {
					        $value = '';
				        }
			        }
		        }
	        } else {
		        $atts = ltrim($text);
	        }

	        return $atts;
        };

        $pattern = $this->get_shortcode_regex($tagnames);
        $content = $this->doShortcodes( preg_replace_callback("/$pattern/", function ($m) use ($shortcode_parse_atts) {
            // allow [[foo]] syntax for escaping a tag
            if ($m[1] == '[' && $m[6] == ']') {
                return substr($m[0], 1, -1);
            }

            $tag = $m[2];
            $attr = $shortcode_parse_atts($m[3]);

            if (!is_callable($this->shortcodes[$tag])) {
                return $m[0];
            }

            $content = isset($m[5]) ? $m[5] : null;

            $output = $m[1] . call_user_func($this->shortcodes[$tag], $attr, $content, $tag) . $m[6];
            return $output;
        }, $content) );

        return $content;
    }

    private function get_shortcode_regex(array $tagnames): string
    {
        if (empty($tagnames)) {
            $tagnames = array_keys($this->shortcodes);
        }
        $tagregexp = join('|', array_map('preg_quote', $tagnames));

        // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
        // Also, see shortcode_unautop() and shortcode.js.

        // phpcs:disable Squiz.Strings.ConcatenationSpacing.PaddingFound -- don't remove regex indentation
        return
            '\\['                                // Opening bracket
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
            . "($tagregexp)"                     // 2: Shortcode name
            . '(?![\\w-])'                       // Not followed by word character or hyphen
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
            . '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            . '(?:'
            . '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            . '[^\\]\\/]*'               // Not a closing bracket or forward slash
            . ')*?'
            . ')'
            . '(?:'
            . '(\\/)'                        // 4: Self closing tag ...
            . '\\]'                          // ... and closing bracket
            . '|'
            . '\\]'                          // Closing bracket
            . '(?:'
            . '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            . '[^\\[]*+'             // Not an opening bracket
            . '(?:'
            . '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            . '[^\\[]*+'         // Not an opening bracket
            . ')*+'
            . ')'
            . '\\[\\/\\2\\]'             // Closing shortcode tag
            . ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
        // phpcs:enable
    }
}