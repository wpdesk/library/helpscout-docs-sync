<?php

namespace WPDesk\HsSync\Dto;

class Collection
{
    public string $path;
    public string $name;

    /**
     * @var Category[]
     */
    public \Generator $categories;

    public function __construct(string $path, string $name, \Generator $categories)
    {
        $this->path = $path;
        $this->name = $name;
        $this->categories = $categories;
    }
}