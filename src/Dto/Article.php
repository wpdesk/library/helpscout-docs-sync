<?php

namespace WPDesk\HsSync\Dto;

class Article
{
    public string $path;
    public string $name;
    public string $text;
    public string $slug;
    /** @var string[] */
    public array $assets;

    public function __construct(string $path, string $name, string $text, string $slug, array $assets = [])
    {
        $this->path = $path;
        $this->name = $name;
        $this->text = $text;
        $this->slug = $slug;
        $this->assets = $assets;
    }
}