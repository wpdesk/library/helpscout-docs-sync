<?php

namespace WPDesk\HsSync\Dto;

class Category
{
    public string $path;
    public string $name;
    public string $description;

    /**
     * @var Article[]
     */
    public \Generator $articles;

    public function __construct(string $path, string $name, string $description, \Generator $articles)
    {
        $this->path = $path;
        $this->name = $name;
        $this->description = $description;
        $this->articles = $articles;
    }


}