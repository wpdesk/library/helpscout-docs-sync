<?php

namespace WPDesk\HsSync;

use WPDesk\HsSync\Dto\Article;
use WPDesk\HsSync\Dto\Category;
use WPDesk\HsSync\Dto\Collection;

/**
 * Can parse local DOCS structure and return it as Collection/Category/Article.
 *
 * @package WPDesk\HsSync
 */
class DocsProvider
{
    private string $basePath;
    private Parser $parser;

    public function __construct(Parser $parser, string $basePath = '.')
    {
        $this->parser = $parser;
        $this->basePath = realpath($basePath);
    }

    private function getCategories(string $path): \Generator
    {
        $files = scandir($path);
        foreach ($files as $file) {
            $fullPath = $path . DIRECTORY_SEPARATOR . $file;
            $categoryInfoFullPath = $fullPath . DIRECTORY_SEPARATOR . 'category.md';
            if (is_dir($fullPath) && is_file($categoryInfoFullPath)) {
                $categoryInfo = file_get_contents($categoryInfoFullPath);
                if ($categoryInfo !== false) {
                    $rawCategoryInfo = explode("\n", $this->parser->parse($categoryInfo));
                    $name = strip_tags($rawCategoryInfo[0]);
                    array_shift($rawCategoryInfo);
                    $description = strip_tags(implode("\n", $rawCategoryInfo));
                    yield new Category($fullPath, $name, $description, $this->getArticles($fullPath));
                }
            }
        }
    }

    private function getAssetsPaths(string $articlePath): array
    {
        $articleAssetsPath = $articlePath . DIRECTORY_SEPARATOR . 'assets';
        if (!file_exists($articleAssetsPath)) {
            return [];
        }
        $assets = scandir($articleAssetsPath);
        if (!is_array($assets)) {
            return [];
        }
        $assets = array_map(function ($item) use ($articleAssetsPath) {
            return $articleAssetsPath . DIRECTORY_SEPARATOR . $item;
        }, $assets);
        $assets = array_filter($assets, 'is_file');
        return $assets;
    }

    private function getArticles(string $path): \Generator
    {
        $files = scandir($path);
        foreach ($files as $file) {
            $fullPath = $path . DIRECTORY_SEPARATOR . $file;
            $articleInfoFullPath = $fullPath . DIRECTORY_SEPARATOR . "$file.md";

            if (is_dir($fullPath) && is_file($articleInfoFullPath)) {
                $articleInfo = file_get_contents($articleInfoFullPath);
                if ($articleInfo !== false) {
                    $rawArticleInfo = explode("\n", $this->parser->parse($articleInfo));
                    $name = strip_tags($rawArticleInfo[0]);
                    array_shift($rawArticleInfo);
                    $text = implode("\n", $rawArticleInfo);
                    $slug = basename($fullPath);
                    $assets = $this->getAssetsPaths($fullPath);

                    yield new Article($fullPath, $name, $text, $slug, $assets);
                }
            }
        }
    }

    /**
     * @return Collection[]
     */
    public function getCollections(): \Generator
    {
        $files = scandir($this->basePath);
        foreach ($files as $file) {
            $fullPath = $this->basePath . DIRECTORY_SEPARATOR . $file;
            $collectionInfoFullPath = $fullPath . DIRECTORY_SEPARATOR . 'collection.md';
            if (is_dir($fullPath) && is_file($collectionInfoFullPath)) {
                $collectionInfo = file_get_contents($collectionInfoFullPath);
                if ($collectionInfo !== false) {
                    $name = strip_tags($this->parser->parse($collectionInfo));
                    yield new Collection($fullPath, $name, $this->getCategories($fullPath));
                }
            }
        }
    }
}