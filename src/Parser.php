<?php

namespace WPDesk\HsSync;

use Parsedown;

/**
 * Can parse MarkDown, insert external assets etc.
 * Use to prepare MD text assets for HS to consume.
 *
 * @package WPDesk\HsSync
 */
class Parser
{
    private Parsedown $parser;

    public function __construct()
    {
        $this->parser = new Parsedown();
    }

    /**
     * Inject/replace external assets url in text content.
     *
     * @param string $txt
     * @param array $assets [url => string, size => getimagesize]
     * @return string
     */
    public function injectAssets(string $txt, array $assets): string
    {
        foreach ($assets as $asset) {
            $placeholder_name = 'assets/' . $asset['basename'];
            // set smaller size when retina image is detected
            if (!empty($asset['size']) && stripos($placeholder_name, '@2x') !== false) {
                $asset['size'][0] = intdiv($asset['size'][0], 2);
                $asset['size'][1] = intdiv($asset['size'][1], 2);
                $replacement = $asset['url'] . "\" width=\"{$asset['size'][0]}\" height=\"{$asset['size'][1]}";
            } else {
                $replacement = $asset['url'];
            }

            $txt = str_replace($placeholder_name, $replacement, $txt);
        }
        return $txt;
    }

    /**
     * Parse WordPress shortcodes.
     *
     * @param string $content
     * @return string
     */
    public function doShortcodes(string $content): string
    {
        return (new ShortCodeParser())->doShortcodes($content);
    }

    /**
     * Add support fox syntax prettifier.
     *
     * @param string $txt
     * @return string
     */
    public function prettify(string $txt): string
    {
        $txt = str_replace('<pre>', '<pre class="prettyprint">', $txt);
        return str_replace('<code>', '<code class="prettyprint">', $txt);
    }

    /**
     * Add target=_blank for external links.
     *
     * @param string $txt
     * @param string[] $internalDomains
     * @return string
     */
    public function blankExternal(string $txt, array $internalDomains): string
    {
        foreach ($internalDomains as $domain) {
            $txt = preg_replace_callback('/href="[^"]+"/', function($item) use ($domain) {
                if (stripos($item[0], $domain) !== false) {
                    return $item[0];
                }
                return ' target="_blank" ' . $item[0];
            }, $txt);
        }
        return $txt;
    }

    /**
     * Parse Markdown and convert it to HTML.
     *
     * @param string $txt
     * @return string
     */
    public function parse(string $txt): string
    {
        return $this->parser->text($txt);
    }
}
