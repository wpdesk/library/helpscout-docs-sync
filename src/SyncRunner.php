<?php

namespace WPDesk\HsSync;

/**
 * Sync routine. Use to sync.
 *
 * @package WPDesk\HsSync
 */
class SyncRunner
{
    private HsApi $hs;

    private DocsProvider $docs;

    private Parser $parser;

    public function __construct(string $apiKey, string $siteId, string $docsDirPath)
    {
        $this->hs = new HsApi($apiKey, $siteId);

        $this->parser = new Parser();
        $this->docs = new DocsProvider($this->parser, $docsDirPath);
    }

    private function print(string $str): void
    {
        echo $str . "\n";
    }

    public function run(): void
    {
        $this->print("Start");
        foreach ($this->docs->getCollections() as $collection) {
            $this->print("Collection: {$collection->name}");
            $remoteCollection = $this->hs->touchCollection($collection);
            foreach ($collection->categories as $category) {
                $this->print("- Category: {$category->name}");
                $remoteCategory = $this->hs->touchCategory($remoteCollection, $category);
                foreach ($category->articles as $article) {
                    $this->print("--- Article: {$article->name}");
                    $remoteArticle = $this->hs->touchArticle($remoteCategory, $article);
                    $assetsUrls = [];
                    foreach ($article->assets as $asset) {
                        $basename = basename($asset);
                        $this->print("----- Asset: {$basename}");
                        $assetsUrls[] = [
                            'basename' => $basename,
                            'size' => getimagesize($asset),
                            'url' => $this->hs->touchAsset($remoteArticle, $asset)->getFileLink()
                        ];
                    }
                    $article->text = $this->parser->injectAssets($article->text, $assetsUrls);
                    $article->text = $this->parser->doShortcodes($article->text);
                    $article->text = $this->parser->prettify($article->text);
                    $article->text = $this->parser->blankExternal($article->text, [
                        'docs.shopmagic.app'
                    ]);
                    $this->hs->touchArticle($remoteCategory, $article);
                }
            }
        }
        $this->print("End");
    }
}