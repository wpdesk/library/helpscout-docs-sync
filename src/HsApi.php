<?php

namespace WPDesk\HsSync;

use HelpScoutDocs\DocsApiClient;
use HelpScoutDocs\Models\Article as RemoteArticle;
use HelpScoutDocs\Models\ArticleAsset;
use HelpScoutDocs\Models\ArticleRef;
use HelpScoutDocs\Models\Category as RemoteCategory;
use HelpScoutDocs\Models\Collection as RemoteCollection;
use HelpScoutDocs\ResourceCollection;
use WPDesk\HsSync\Dto\Article;
use WPDesk\HsSync\Dto\Category;
use WPDesk\HsSync\Dto\Collection;

/**
 * Can connect to HS DOCS API and push Collection/Category/Article structure into HS.
 *
 * @package WPDesk\HsSync
 */
class HsApi
{
    private DocsApiClient $api;
    private string $apiKey;
    private string $siteId;

    /**
     * HsApi constructor.
     * @param string $apiKey
     * @param string $siteId
     */
    public function __construct(string $apiKey, string $siteId)
    {
        $this->apiKey = $apiKey;
        $this->siteId = $siteId;

        $this->api = new DocsApiClient($this->apiKey);
    }

    /**
     * Ensures than an collection is uploaded. To identify an collection a name is used.
     *
     * @param Collection $collection
     * @return RemoteCollection
     */
    public function touchCollection(Collection $collection): RemoteCollection
    {
        $remoteCollection = $this->findRemoteCollection($collection);
        if ($remoteCollection === null) {
            return $this->createRemoteCollection($collection);
        }
        return $remoteCollection;
    }

    private function findRemoteCollection(Collection $collection): ?RemoteCollection
    {
        $collections = $this->getRemoteCollections();
        return array_reduce($collections, function ($curr, $item) use ($collection) {
            /** @var RemoteCollection $item */
            if ($item->getName() === $collection->name) {
                return $item;
            }
            return $curr;
        }, null);
    }

    private function getRemoteCollections(): array
    {
        $remoteCollections = $this->getAllPages(function ($page) {
            return $this->api->getCollections($page, $this->siteId);
        });
        if ($remoteCollections !== false) {
            // there is a bug in api. We get all collections so we have to filter ours.
            $remoteCollections = array_filter($remoteCollections, function ($item) {
                /** @var RemoteCollection $item */
                return $item->getSiteId() === $this->siteId;
            });
        }
        return $remoteCollections;
    }

    /**
     * Can load all data pages into one array.
     *
     * @param callable $apiCall Api Call to HS: function(int $page): ResourceCollection
     * @return array
     */
    private function getAllPages(callable $apiCall): array
    {
        $page = 0;
        $results = [];
        do {
            /** @var ResourceCollection $resourceCollection */
            $resourceCollection = $apiCall($page++);
			try {
				$results[] = (array) $resourceCollection->getItems();
			} catch (\Throwable $t) {
				//
			}
        } while ($resourceCollection->hasNextPage());
        return array_merge(...$results);
    }

    private function createRemoteCollection(Collection $collection): RemoteCollection
    {
        $newRemoteCollection = new RemoteCollection();
        $newRemoteCollection->setSiteId($this->siteId);
        $newRemoteCollection->setName($collection->name);
        return $this->api->createCollectionAndReturnCreated($newRemoteCollection);
    }

    /**
     * Ensures than a categoru is uploaded. To identify an article a name is used.
     *
     * @param RemoteCollection $remoteCollection
     * @param Category $category
     * @return RemoteCategory
     */
    public function touchCategory(RemoteCollection $remoteCollection, Category $category): RemoteCategory
    {
        $remoteCategory = $this->findRemoteCategory($remoteCollection, $category);
        if ($remoteCategory === null) {
            return $this->createRemoteCategory($remoteCollection, $category);
        }
        return $this->updateRemoteCategory($remoteCategory, $category);
    }

    private function findRemoteCategory(RemoteCollection $remoteCollection, Category $category): ?RemoteCategory
    {
        $remoteCategories = $this->getAllPages(function ($page) use ($remoteCollection) {
            return $this->api->getCategories($remoteCollection->getId(), $page);
        });
        return array_reduce($remoteCategories, static function ($curr, $item) use ($category) {
            /** @var RemoteCategory $item */
            if ($item->getName() === $category->name) {
                return $item;
            }
            return $curr;
        }, null);
    }

    private function createRemoteCategory(RemoteCollection $remoteCollection, Category $category): RemoteCategory
    {
        $newRemoteCategory = new RemoteCategory();
        $newRemoteCategory->setCollectionId($remoteCollection->getId());
        $newRemoteCategory->setName($category->name);
        $newRemoteCategory->setDescription($category->description);
        return $this->api->createCategoryAndReturnCreated($newRemoteCategory);
    }

    private function updateRemoteCategory(RemoteCategory $remoteCategory, Category $category): RemoteCategory
    {
        $remoteCategory->setName($category->name);
        $remoteCategory->setDescription($category->description);
        return $this->api->updateCategoryAndReturnUpdated($remoteCategory);
    }

    /**
     * Ensures than an asset is uploaded.
     *
     * @param RemoteArticle $remoteArticle
     * @param string $path Path to file.
     * @return ArticleAsset
     * @throws \HelpScoutDocs\ApiException
     */
    public function touchAsset(RemoteArticle $remoteArticle, string $path): ArticleAsset
    {
        $articleAsset = new ArticleAsset();
        if (getimagesize($path) !== false) {
            $articleAsset->setAssetType(ArticleAsset::ARTICLE_ASSET_IMAGE);
        } else {
            $articleAsset->setAssetType(ArticleAsset::ARTICLE_ASSET_ATTACHMENT);
        }
        $articleAsset->setArticleId($remoteArticle->getId());

        // path for invalid @2x in S3
        try {
            $sanitizedPath = $this->getSanitizedFilePath($path);
            if ($path !== $sanitizedPath) {
                copy($path, $sanitizedPath);
                $articleAsset->setFile($sanitizedPath);
            } else {
                $articleAsset->setFile($path);
            }
            return $this->api->createArticleAsset($articleAsset);
        } finally {
            if ($path !== $sanitizedPath) {
                unlink($sanitizedPath);
            }
        }
    }

    /**
     * Remove unwanted chars from file name.
     *
     * @param string $path
     * @return string
     */
    private function getSanitizedFilePath(string $path): string
    {
        return str_replace('@2x', '', $path);
    }

    /**
     * Ensures than an article is uploaded. To identify an article a slug is used.
     *
     * @param RemoteCategory $remoteCategory
     * @param Article $article
     * @return RemoteArticle
     */
    public function touchArticle(RemoteCategory $remoteCategory, Article $article): RemoteArticle
    {
        $remoteArticle = $this->findRemoteArticle($remoteCategory, $article);
        if ($remoteArticle === null) {
            return $this->createRemoteArticle($remoteCategory, $article);
        }
        return $this->updateRemoteArticle($this->convertArticleRefToArticle($remoteArticle), $remoteCategory, $article);
    }

    private function findRemoteArticle(RemoteCategory $remoteCategory, Article $article): ?ArticleRef
    {
        $remoteArticles = $this->getAllPages(function ($page) use ($remoteCategory) {
            return $this->api->getArticlesForCategory($remoteCategory->getId(), $page);
        });
        return array_reduce($remoteArticles, static function ($curr, $item) use ($article) {
            /** @var RemoteArticle $item */
            if ($item->getSlug() === $article->slug) {
                return $item;
            }
            return $curr;
        }, null);
    }

    private function createRemoteArticle(RemoteCategory $remoteCategory, Article $article): RemoteArticle
    {
        $newRemoteArticle = new RemoteArticle();
        $newRemoteArticle->setName($article->name);
        $newRemoteArticle->setCollectionId($remoteCategory->getCollectionId());
        $newRemoteArticle->setCategories([$remoteCategory->getId()]);
        $newRemoteArticle->setText($article->text);
        $newRemoteArticle->setSlug($article->slug);
        $newRemoteArticle->setStatus(RemoteArticle::ARTICLE_STATUS_PUBLISHED);
        return $this->api->createArticleAndReturnCreated($newRemoteArticle);
    }

    private function updateRemoteArticle(
        RemoteArticle $remoteArticle,
        RemoteCategory $remoteCategory,
        Article $article
    ): RemoteArticle {
        $remoteArticle->setName($article->name);
        $remoteArticle->setCollectionId($remoteCategory->getCollectionId());
        $remoteArticle->setCategories([$remoteCategory->getId()]);
        $remoteArticle->setText($article->text);
        $remoteArticle->setStatus(RemoteArticle::ARTICLE_STATUS_PUBLISHED);
        $this->api->updateArticleAndReturnUpdated($remoteArticle);
		return $remoteArticle;
    }

    private function convertArticleRefToArticle(ArticleRef $ref): RemoteArticle
    {
        $article = new RemoteArticle();
        $article->setName($ref->getName());
        $article->setCollectionId($ref->getCollectionId());
        $article->setId($ref->getId());
        $article->setSlug($ref->getSlug());
        return $article;
    }
}