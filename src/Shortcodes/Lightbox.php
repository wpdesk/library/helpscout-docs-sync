<?php

declare(strict_types=1);

namespace WPDesk\HsSync\Shortcodes;

/**
 * Can handle lightbox shortcode.
 */
final class Lightbox {

	use AttributesParser;

	/**
	 * @param array|string $attributes
	 * @param string|null  $content
	 *
	 * @return string
	 */
	public function do_shortcode( $attributes, string $content = null ): string {

		return sprintf(
			'<a href="%1$s" class="lightbox">%2$s</a>',
			$this->get_image_url_from_content( $content ?? '' ),
			$content ?? ''
		);
	}

	private function get_image_url_from_content( string $content ): string {
		preg_match( '%<img.*?src=["\'](.*?)["\'].*?/>%i', $content, $matches );
		$url = isset( $matches[1] ) ? $matches[1] : '';

		return $url;
	}

}
