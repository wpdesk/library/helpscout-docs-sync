<?php

declare(strict_types=1);

namespace WPDesk\HsSync\Shortcodes;

/**
 * Can handle Button shortcode.
 */
final class Div {

	use AttributesParser;

	public function do_shortcode( array $attributes, string $content = null ): string {
		$attributes = $this->parse_attributes( array_merge( [ 'class' => 'div' ], $attributes ) );

		$content = sprintf(
			'<div class="%1$s">%2$s</div>',
			$attributes['class'],
			$content
		);
		return $content;
	}

}
