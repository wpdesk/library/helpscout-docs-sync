<?php

declare(strict_types=1);

namespace WPDesk\HsSync\Shortcodes;

/**
 * Can handle Box shortcode.
 */
final class Box {

	use AttributesParser;

	private const CLASSES = [
		'info' => 'callout-blue',
		'warning' => 'callout-yellow',
		'error' => 'callout-red',
	];

	private const DEFAULT_TYPE = 'info';

	public function do_shortcode( array $attributes, string $content = null ): string {
		$attributes = $this->parse_attributes( array_merge( [ 'type' => 'info' ], $attributes ) );

		return sprintf(
			'<section class="%1$s">%2$s</section>',
			$this->get_css_class( $attributes['type'] ),
			$content ?? ''
		);
	}

	private function get_css_class( string $type = null ): string {
		$type = ( null !== $type && isset( self::CLASSES[ $type ] ) ) ? $type : self::DEFAULT_TYPE;

		return self::CLASSES[ $type ];
	}

}
