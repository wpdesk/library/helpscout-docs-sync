<?php

namespace WPDesk\HsSync\Shortcodes;

/**
 * Can parse attributes.
 */
trait AttributesParser {

	private function parse_attributes( array $attributes ): array {
		return array_map( function( $attribute ) {
			return trim( $attribute, '&quot;' );
		}, $attributes );
	}

}