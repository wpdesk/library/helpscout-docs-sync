<?php

declare(strict_types=1);

namespace WPDesk\HsSync\Shortcodes;

/**
 * Can handle Button shortcode.
 */
final class Button {

	use AttributesParser;

	public function do_shortcode( array $attributes, string $content = null ): string {
		$attributes = $this->parse_attributes( array_merge( [ 'type' => 'solid' ], $attributes ) );

		$content = sprintf(
			'<span class="%1$s">%2$s</span>',
			$this->get_css_class( $attributes['type'] ),
			$content
		);
		return $content;
	}

	private function get_css_class( string $type = null ): string {
		return 'button-' . $type;
	}

}
