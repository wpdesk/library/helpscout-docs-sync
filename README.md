# HelpScout Docs Sync

This library can upload documentation in markdown format to HelpScout Docs.

Documentation format: https://wpdesk.slab.com/posts/dokumentacja-w-git-labie-zwqmz8lk

## Requirements

Library requires PHP 7.4.

### Api Key

Should be generated using HS account with Owner capabilities. Instruction how to acquire key is here https://developer.helpscout.com/docs-api/

### Site ID

Get it from HelpScout admin url. For example when docs URL is https://secure.helpscout.net/settings/docs/site/5f8566584cedfd0017dd1112
then the Site ID: 5f8566584cedfd0017dd1112


## Usage

This code is using HELPSCOUT_API_KEY/HELPSCOUT_DOCS_SITE_ID from command line and
when the command line args are not provided then from Gitlab CI variables.
```
<?php
require_once __DIR__ . '/vendor/autload.php';

$apiKey = empty($argv[1]) ? $_SERVER['HELPSCOUT_API_KEY'] : $argv[1];
$docsSite = empty($argv[2]) ? $_SERVER['HELPSCOUT_DOCS_SITE_ID'] : $argv[2];

(new \WPDesk\HsSync\SyncRunner($apiKey, $docsSite, __DIR__))
    ->run();
```

Simple Gitlab CI script:

```
stages:
  - sync

before_script:
  - mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
  - cd hs-sync
  - apt-get update && apt-get install -y libzip-dev git
  - docker-php-ext-install zip
  - php -r "copy('https://getcomposer.org/download/1.10.6/composer.phar', 'composer.phar');"
  - php composer.phar install --no-dev

sync with hs prod:
    tags:
      - deploy
    image: php:7.4-cli
    stage: sync
    script:
      - php sync.php
    only:
      - master
```