## [1.4.0] - 2022-12-10
### Fixed
- Creating article
### Changed
- wpdesk helpscout-docs-api-php with our fixes

## [1.3] - 2022-11-22
### Changed
- helpscout-docs-api-php API ^3

## [1.2.1] - 2021-08-24
### Fixed
- prettify support without breaking html validity

## [1.2.0] - 2021-08-24
### Added
- div shortcode

## [1.1.0] - 2020-12-23
### Added
- nested shortcodes

## [1.0.6] - 2020-12-23
### Fixed
- changed shortcodes execution order

## [1.0.5] - 2020-12-10
### Added
- lightbox shortcode

## [1.0.4] - 2020-12-10
### Added
- shortcode attributes parser

## [1.0.3] - 2020-12-10
### Fixed
- PHP 7.4 code improvements

## [1.0.2] - 2020-12-09
### Added
- button shortcode
- box shortcode

## [1.0.1] - 2020-11-16
### Fixed
- Cannot redeclare function in ShortCodeParser

## [1.0.0] - 2020-11-16
### Added
- First version
